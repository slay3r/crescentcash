package app.crescentcash.src.activity

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.SimpleAdapter
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import app.crescentcash.src.R
import app.crescentcash.src.manager.UIManager
import app.crescentcash.src.manager.WalletManager
import app.crescentcash.src.ui.NonScrollListView
import app.crescentcash.src.utils.Constants
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.luminiasoft.ethereum.blockiesandroid.BlockiesIdenticon
import java.math.BigDecimal
import java.math.RoundingMode

import kotlin.collections.set

class TokensListActivity : AppCompatActivity() {
    private lateinit var srlSLP: SwipeRefreshLayout
    private lateinit var fabReceiveSLP: FloatingActionButton
    private lateinit var slpList: NonScrollListView
    lateinit var bchBalanceSLP: TextView

    private lateinit var no_tx_text_slp: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        UIManager.determineTheme(this)
        this.setContentView(R.layout.slp_balances)
        this.findViews()
        this.prepareViews(savedInstanceState)
        this.initListeners()
    }

    private fun prepareViews(savedInstanceState: Bundle?) {
        WalletManager.getSLPWallet().refreshBalance()
        bchBalanceSLP.text = if (savedInstanceState == null) {
            val extras = intent.extras
            if (extras == null) {
                ""
            } else {
                extras.getString(Constants.INTENT_BALANCE_DATA)
            }
        } else {
            savedInstanceState.getSerializable(Constants.INTENT_BALANCE_DATA).toString()
        }

        this.refreshSLP()
    }

    private fun findViews() {
        no_tx_text_slp = this.findViewById(R.id.no_tx_text_slp)
        fabReceiveSLP = this.findViewById(R.id.fabReceiveSLP)
        bchBalanceSLP = this.findViewById(R.id.bchBalanceSLP)
        srlSLP = this.findViewById(R.id.srlSLP)
        slpList = this.findViewById(R.id.slpList)
    }

    private fun initListeners() {
        WalletManager.getSLPWallet().balance.observe(this, Observer { data -> WalletManager.balances = data })

        this.srlSLP.setOnRefreshListener { this.refreshSLP() }
        this.fabReceiveSLP.setOnClickListener {
            UIManager.startActivity(this, ReceiveSLPActivity::class.java)
        }
        this.slpList.setOnItemClickListener { parent, view, position, id ->
            WalletManager.currentTokenPosition = position
            WalletManager.currentTokenId = WalletManager.tokenListInfo[WalletManager.currentTokenPosition].tokenId

            val sendSlpActivity = Intent(this, SendSLPActivity::class.java)
            sendSlpActivity.putExtra(Constants.INTENT_TOKEN_TICKER_DATA, WalletManager.tokenListInfo[WalletManager.currentTokenPosition].ticker)
            this.startActivity(sendSlpActivity)
        }
    }

    fun refreshSLP() {
        setSLPList()

        if (srlSLP.isRefreshing) srlSLP.isRefreshing = false
    }

    private fun setSLPList() {
        WalletManager.getSLPWallet().refreshBalance()
        if (WalletManager.balances.isNotEmpty()) {
            WalletManager.tokenList = ArrayList()
            WalletManager.tokenListInfo = ArrayList()
            val amtOfTokens = WalletManager.balances.size

            if (amtOfTokens >= 2) {
                srlSLP.visibility = View.VISIBLE
                no_tx_text_slp.visibility = View.GONE

                for (x in 0 until amtOfTokens) {
                    /*
                    SLP SDK includes BCH in the balance list, with a tokenHash of "" so we check shortly after gathering all of the data if the hash does not equal "" before adding
                     */
                    val datum = HashMap<String, String>()
                    val tokenName = WalletManager.balances[x].name
                    val tokenTicker = WalletManager.balances[x].ticker
                    val tokenHash = WalletManager.balances[x].tokenId
                    val balance = WalletManager.balances[x].amount.toPlainString()
                    val amt = BigDecimal(java.lang.Double.parseDouble(balance)).setScale(WalletManager.balances[x].decimals!!, RoundingMode.HALF_UP)

                    datum["token"] = tokenName!!
                    datum["tokenHash"] = tokenHash
                    datum["tokenTicker"] = tokenTicker!!
                    datum["balance"] = amt.toPlainString()

                    if (tokenHash != "") {
                        WalletManager.tokenList.add(datum)
                        WalletManager.tokenListInfo.add(WalletManager.balances[x])
                    }
                }

                val itemsAdapter = object : SimpleAdapter(this, WalletManager.tokenList, R.layout.list_view_activity, null, null) {
                    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                        // Get the Item from ListView
                        val view = LayoutInflater.from(this@TokensListActivity).inflate(R.layout.list_view_activity, null)
                        val slpBlockiesAddress = WalletManager.blockieAddressFromTokenId(WalletManager.tokenList[position]["tokenHash"]
                                ?: error(""))

                        val slpImage = view.findViewById<BlockiesIdenticon>(R.id.slpImage)
                        val slpIcon = view.findViewById<ImageView>(R.id.slpWithIcon)

                        object : Thread() {
                            override fun run() {
                                try {
                                    val tokenHash = WalletManager.tokenList[position]["tokenHash"]
                                    val exists = this@TokensListActivity.resources.getIdentifier("slp$tokenHash", "drawable", this@TokensListActivity.packageName) != 0
                                    if (exists) {
                                        val drawable = this@TokensListActivity.resources.getDrawable(this@TokensListActivity.resources.getIdentifier("slp$tokenHash", "drawable", this@TokensListActivity.packageName))
                                        this@TokensListActivity.runOnUiThread {
                                            slpIcon.setImageDrawable(drawable)
                                            slpImage.visibility = View.GONE
                                            slpIcon.visibility = View.VISIBLE
                                        }
                                    } else {
                                        slpImage.setAddress(slpBlockiesAddress)
                                        slpImage.setCornerRadius(128f)
                                    }
                                } catch (e: Exception) {
                                    slpImage.setAddress(slpBlockiesAddress)
                                    slpImage.setCornerRadius(128f)
                                }
                            }
                        }.start()

                        // Initialize a TextView for ListView each Item
                        val text1 = view.findViewById<TextView>(R.id.text1)
                        val text2 = view.findViewById<TextView>(R.id.text2)
                        val text3 = view.findViewById<TextView>(R.id.text3)

                        // Set the text color of TextView (ListView Item)

                        text1.text = WalletManager.tokenList[position]["token"].toString()
                        text2.text = WalletManager.tokenList[position]["balance"].toString()
                        text3.text = WalletManager.tokenList[position]["tokenTicker"].toString()

                        if (UIManager.nightModeEnabled) {
                            text1.setTextColor(Color.WHITE)
                            text2.setTextColor(Color.GRAY)
                            text3.setTextColor(Color.WHITE)
                        } else {
                            text1.setTextColor(Color.BLACK)
                            text3.setTextColor(Color.BLACK)
                        }

                        text2.ellipsize = TextUtils.TruncateAt.END
                        text2.maxLines = 1
                        text2.isSingleLine = true
                        // Generate ListView Item using TextView
                        return view
                    }
                }
                this.runOnUiThread {
                    slpList.adapter = itemsAdapter
                    slpList.refreshDrawableState()
                }
            } else {
                srlSLP.visibility = View.GONE
                no_tx_text_slp.visibility = View.VISIBLE
                this.runOnUiThread {
                    slpList.adapter = null
                    slpList.refreshDrawableState()
                }
            }
        }
    }

    companion object
}
