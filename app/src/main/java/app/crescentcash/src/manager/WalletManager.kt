package app.crescentcash.src.manager

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.CountDownTimer
import android.os.Handler
import android.os.Looper
import android.os.Vibrator
import android.text.TextUtils
import android.view.View
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import app.crescentcash.src.MainActivity
import app.crescentcash.src.R
import app.crescentcash.src.activity.DecryptWalletActivity
import app.crescentcash.src.activity.PaymentReceivedActivity
import app.crescentcash.src.activity.SendActivity
import app.crescentcash.src.activity.SendSLPActivity
import app.crescentcash.src.uri.URIHelper
import app.crescentcash.src.utils.Constants
import app.crescentcash.src.utils.OpPushParser
import app.crescentcash.src.utils.PrefsUtil
import app.crescentcash.src.utils.UtxoUtil
import com.bitcoin.slpwallet.Network
import com.bitcoin.slpwallet.SLPWallet
import com.bitcoin.slpwallet.SLPWalletConfig
import com.bitcoin.slpwallet.presentation.BalanceInfo
import com.google.common.collect.ImmutableList
import com.google.common.util.concurrent.FutureCallback
import com.google.common.util.concurrent.Futures
import com.google.common.util.concurrent.ListenableFuture
import com.google.protobuf.ByteString
import com.vdurmont.emoji.EmojiParser
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import org.bitcoinj.core.*
import org.bitcoinj.core.listeners.DownloadProgressTracker
import org.bitcoinj.crypto.BIP38PrivateKey
import org.bitcoinj.crypto.KeyCrypterScrypt
import org.bitcoinj.kits.WalletAppKit
import org.bitcoinj.net.discovery.PeerDiscovery
import org.bitcoinj.params.MainNetParams
import org.bitcoinj.params.TestNet3Params
import org.bitcoinj.protocols.payments.PaymentProtocol
import org.bitcoinj.protocols.payments.PaymentProtocolException
import org.bitcoinj.protocols.payments.PaymentSession
import org.bitcoinj.script.ScriptBuilder
import org.bitcoinj.script.ScriptOpCodes
import org.bitcoinj.utils.MonetaryFormat
import org.bitcoinj.utils.Threading
import org.bitcoinj.wallet.DeterministicSeed
import org.bitcoinj.wallet.Protos
import org.bitcoinj.wallet.SendRequest
import org.bitcoinj.wallet.Wallet
import org.spongycastle.crypto.params.KeyParameter
import org.spongycastle.util.encoders.Hex
import java.io.File
import java.math.BigDecimal
import java.net.InetAddress
import java.net.InetSocketAddress
import java.net.UnknownHostException
import java.nio.charset.StandardCharsets
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*
import java.util.concurrent.ExecutionException
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit

class WalletManager {
    companion object {
        val walletDir: File = File(MainActivity.getDataDir())
        lateinit var currentEcKey: ECKey
        var selectedUtxos: ArrayList<TransactionOutput> = ArrayList()
        lateinit var balances: List<BalanceInfo>
        var tokenList = ArrayList<Map<String, String>>()
        lateinit var tokenListInfo: ArrayList<BalanceInfo>
        lateinit var currentTokenId: String
        var currentTokenPosition: Int = 0
        var walletKit: WalletAppKit? = null
        var slpWallet: SLPWallet? = null
        var aesKey: KeyParameter? = null
        var parameters: NetworkParameters = if (Constants.IS_PRODUCTION) MainNetParams.get() else TestNet3Params.get()
        var addOpReturn: Boolean = false
        var encrypted: Boolean = false
        var useTor: Boolean = false
        var allowLegacyP2SH: Boolean = false
        lateinit var sendType: String
        lateinit var displayUnits: String
        lateinit var bchToSend: String
        var maximumAutomaticSend: Float = 0.0f
        var registeredTxHash = "null"
        var registeredBlock = "null"
        var registeredBlockHash = "null"
        lateinit var timer: CountDownTimer
        var downloading: Boolean = false
        lateinit var issuedKeysArrayList: ArrayList<Map<String, ECKey>>
        val utxoMap = ArrayList<Map<String, TransactionOutput>>()
        lateinit var txList: ArrayList<Transaction>

        val SCRYPT_PARAMETERS: Protos.ScryptParameters = Protos.ScryptParameters.newBuilder().setP(6).setR(8).setN(32768).setSalt(ByteString.copyFrom(KeyCrypterScrypt.randomSalt())).build()

        val wallet: Wallet
            get() = walletKit!!.wallet()

        fun setBitcoinSDKThread() {
            val handler = Handler()
            Threading.USER_THREAD = Executor { handler.post(it) }
        }

        fun getXpub(): String {
            return wallet.watchingKey.serializePubB58(parameters)
        }

        fun isAddressMine(address: String): Boolean {
            val addressObj = AddressFactory.create().getAddress(parameters, address)

            return wallet.isPubKeyHashMine(addressObj.hash160)
        }

        fun getBalance(wallet: Wallet): Coin {
            return wallet.getBalance(Wallet.BalanceType.ESTIMATED)
        }

        fun getWalletKeys(): List<ECKey> {
            return this.wallet.issuedReceiveKeys
        }

        fun getPrivateKey(index: Int): ECKey {
            val keys = getWalletKeys()
            return keys[index]
        }

        fun sweepWallet(activity: Activity, privKey: String) {
            object : Thread() {
                override fun run() {
                    val utxoUtil = UtxoUtil()
                    val key = getPrivateKeyFromString(privKey)
                    utxoUtil.setupSweepWallet(key)
                    val txCount = utxoUtil.getUtxos()
                    if (txCount > 0) {
                        utxoUtil.sweep(wallet.currentReceiveAddress().toString())
                    }

                    val intent = Intent(Constants.ACTION_CLEAR_SWEEP_TEXT)
                    LocalBroadcastManager.getInstance(activity).sendBroadcast(intent)
                }
            }.start()
        }

        fun getPrivateKeyFromString(privKey: String): ECKey {
            val key = if (privKey.length == 51 || privKey.length == 52) {
                val dumpedPrivateKey = DumpedPrivateKey.fromBase58(MainNetParams.get(), privKey)
                dumpedPrivateKey.key
            } else {
                val privateKey = Base58.decodeToBigInteger(privKey)
                ECKey.fromPrivate(privateKey)
            }

            return key
        }

        fun signMessageWithKey(ecKey: ECKey, message: String): String {
            return ecKey.signMessage(message, this.aesKey)
        }

        fun isSignatureValid(address: String, signature: String, message: String): Boolean {
            try {
                val finalAddress = if (address.contains("#")) {
                    org.bitcoinj.net.NetHelper().getCashAccountAddress(this.parameters, address)
                } else {
                    address
                }
                println(finalAddress)
                val signedAddress = ECKey.signedMessageToKey(message, signature).toAddress(MainNetParams.get()).toString()
                val addressLegacy = if (Address.isValidCashAddr(MainNetParams.get(), finalAddress)) {
                    Address.fromCashAddr(MainNetParams.get(), finalAddress).toBase58()
                } else {
                    finalAddress
                }
                println(addressLegacy)
                println(signedAddress == addressLegacy)

                return signedAddress == addressLegacy
            } catch (e: Exception) {
                return false
            }
        }

        fun setupNodeOnStart() {
            val nodeIP = PrefsUtil.prefs.getString("networkNode", "")
            println("GETTING NODE IP: $nodeIP")
            assert(nodeIP != null)
            if (!nodeIP.equals("")) {
                this.walletKit!!.setPeerNodes(null)
                val peerDiscovery: PeerDiscovery = object : PeerDiscovery {
                    override fun getPeers(p0: Long, p1: Long, p2: TimeUnit?): Array<InetSocketAddress>? {
                        return null
                    }

                    override fun shutdown() {

                    }
                }
                this.walletKit!!.setDiscovery(peerDiscovery)
                var node1: InetAddress? = null

                try {
                    node1 = InetAddress.getByName(nodeIP)
                } catch (e: UnknownHostException) {
                    e.printStackTrace()
                }

                this.walletKit!!.setPeerNodes(PeerAddress(node1, if (Constants.IS_PRODUCTION) 8333 else 18333))
            }
        }

        fun importPrivateKey(privKey: String) {
            val key = getPrivateKeyFromString(privKey)

            if (this.wallet.isEncrypted)
                this.wallet.importKeysAndEncrypt(listOf(key), this.aesKey)
            else
                this.wallet.importKey(key)
        }

        fun registerCashAccount(ecKey: ECKey, name: String): String {
            val req = SendRequest.createCashAccount(this.parameters, ecKey.toAddress(this.parameters).toBase58(), name)
            req.ensureMinRequiredFee = false
            req.aesKey = this.aesKey
            req.feePerKb = Coin.valueOf(java.lang.Long.parseLong(1.toString() + "") * 1000L)
            val tx = this.walletKit!!.wallet().sendCoinsOffline(req)
            this.broadcastTxToPeers(tx)
            return tx.hashAsString
        }

        fun verify(activity: Activity, address: String, signature: String, message: String) {
            val isVerified = isSignatureValid(address, signature, message)

            activity.runOnUiThread {
                if (isVerified) {
                    UIManager.showToastMessage(activity, "Signature is valid!")
                } else {
                    UIManager.showToastMessage(activity, "Signature is NOT valid!")
                }
            }
        }

        fun getMaxValueOfSelectedUtxos(): Coin {
            var utxoAmount = 0.0
            for (x in 0 until this.selectedUtxos.size) {
                val utxo = this.selectedUtxos[x]
                utxoAmount += java.lang.Double.parseDouble(utxo.value.toPlainString())
            }

            val str = UIManager.formatBalanceNoUnit(utxoAmount, "#.########")
            return Coin.parseCoin(str)
        }

        fun isProtocol(tx: Transaction, protocolId: String): Boolean {
            for (x in 0 until tx.outputs.size) {
                val output = tx.outputs[x]
                if (output.scriptPubKey.isOpReturn) {
                    if (output.scriptPubKey.chunks[1].data != null) {
                        val protocolCode = String(Hex.encode(output.scriptPubKey.chunks[1].data!!), StandardCharsets.UTF_8)
                        return protocolCode == protocolId
                    }
                }
            }

            return false
        }

        fun sentToSatoshiDice(tx: Transaction): Boolean {
            for (x in 0 until tx.outputs.size) {
                val output = tx.outputs[x]
                if (output.scriptPubKey.isSentToAddress) {
                    val addressP2PKH = output.getAddressFromP2PKHScript(this.parameters).toString()

                    if (Address.isValidLegacyAddress(this.parameters, addressP2PKH) && !this.isAddressMine(addressP2PKH)) {
                        val satoshiDiceAddrs = arrayListOf(
                                "1DiceoejxZdTrYwu3FMP2Ldew91jq9L2u", "1Dice115YcjDrPM9gXFW8iFV9S3j9MtERm",
                                "1Dice1FZk6Ls5LKhnGMCLq47tg1DFG763e", "1Dice1cF41TGRLoCTbtN33DSdPtTujzUzx",
                                "1Dice1wBBY22stCobuE1LJxHX5FNZ7U97N", "1Dice5ycHmxDHUFVkdKGgrwsDDK1mPES3U",
                                "1Dice7JNVnvzyaenNyNcACuNnRVjt7jBrC", "1Dice7v1M3me7dJGtTX6cqPggwGoRADVQJ",
                                "1Dice2wTatMqebSPsbG4gKgT3HfHznsHWi", "1Dice81SKu2S1nAzRJUbvpr5LiNTzn7MDV",
                                "1Dice9GgmweQWxqdiu683E7bHfpb7MUXGd"
                        )
                        return satoshiDiceAddrs.indexOf(addressP2PKH) != -1
                    }
                }
            }

            return false
        }

        fun isCashShuffle(tx: Transaction): Boolean {
            if (tx.outputs.size >= tx.inputs.size && tx.outputs.size > 1 && tx.inputs.size > 1) {
                val shuffledOutputs = ArrayList<String>()
                for (x in 0 until tx.inputs.size) {
                    if (tx.outputs[x].scriptPubKey.isSentToAddress)
                        shuffledOutputs.add(tx.outputs[x].value.toPlainString())
                    else
                        return false
                }

                val hashSet = HashSet(shuffledOutputs)
                return hashSet.size == 1
            }

            return false
        }

        fun broadcastTxToPeers(sendActivity: SendActivity?, tx: Transaction) {
            for (peer in walletKit!!.peerGroup().connectedPeers) {
                peer.sendMessage(tx)
            }

            getSLPWallet().refreshBalance()

            sendActivity?.runOnUiThread {
                sendActivity.displayMyBalance(this.getBalance(wallet).toFriendlyString())
            }
        }

        fun broadcastTxToPeers(tx: Transaction) {
            this.broadcastTxToPeers(null, tx)
        }

        fun setupWalletKit(activity: MainActivity, seed: DeterministicSeed?, cashAcctName: String, verifyingRestore: Boolean) {
            setBitcoinSDKThread()

            walletKit = object : WalletAppKit(parameters, walletDir, Constants.WALLET_NAME) {
                override fun onSetupCompleted() {
                    peerGroup().setBloomFilterFalsePositiveRate(0.01)
                    peerGroup().isBloomFilteringEnabled = true
                    wallet().allowSpendingUnconfirmedTransactions()
                    wallet().isAcceptRiskyTransactions = true
                    setupWalletListeners(activity, wallet())

                    if (MainActivity.isNewUser) {
                        val address = wallet().currentReceiveAddress().toString()

                        println("Registering...")
                        PrefsUtil.prefs.edit().putBoolean("isNewUser", false).apply()
                        if (Constants.IS_PRODUCTION) NetManager.registerCashAccount(activity, cashAcctName, address)
                    } else {
                        val keychainSeed = wallet().keyChainSeed

                        if (keychainSeed.isEncrypted) {
                            /*
                            If the saved setting we got is false, but our wallet is encrypted, then we set our saved setting to true.
                             */
                            if (!encrypted) {
                                encrypted = true
                                PrefsUtil.prefs.edit().putBoolean("useEncryption", encrypted).apply()
                            }

                            UIManager.startActivity(activity, DecryptWalletActivity::class.java)
                        } else {
                            /*
                            If the saved setting we got is true, but our wallet is unencrypted, then we set our saved setting to false.
                             */
                            if (encrypted) {
                                encrypted = false
                                PrefsUtil.prefs.edit().putBoolean("useEncryption", encrypted).apply()
                            }
                        }

                        if (!verifyingRestore) {
                            val hasSLPWallet = PrefsUtil.prefs.getBoolean("hasSLPWallet", false)

                            if (!hasSLPWallet) {
                                val mainHandler = Handler(Looper.getMainLooper())

                                val myRunnable = { setupSLPWallet(activity, null, false) }
                                mainHandler.post(myRunnable)
                            }

                            if (seed == null)
                                activity.refresh()
                        }
                    }
                }
            }

            setupNodeOnStart()

            walletKit!!.setDownloadListener(object : DownloadProgressTracker() {
                override fun progress(pct: Double, blocksSoFar: Int, date: Date) {
                    super.progress(pct, blocksSoFar, date)
                    val percentage = pct.toInt()
                    activity.runOnUiThread {
                        activity.displayPercentage(percentage)
                    }
                }

                override fun doneDownload() {
                    super.doneDownload()
                    activity.runOnUiThread {
                        activity.displayDownloadContent(false)
                        activity.refresh()
                    }
                }
            })

            if (seed != null)
                walletKit!!.restoreWalletFromSeed(seed)

            walletKit!!.setBlockingStartup(false)
            walletKit!!.startAsync()
        }

        private fun setupWalletListeners(activity: MainActivity, wallet: Wallet) {
            wallet.addCoinsReceivedEventListener { wallet1, tx, prevBalance, newBalance ->
                if (!UIManager.isDisplayingDownload) {
                    activity.displayMyBalance(getBalance(wallet).toFriendlyString())

                    if (tx.purpose == Transaction.Purpose.UNKNOWN) {
                        UIManager.playAudio(activity, R.raw.coins_received)
                        UIManager.startActivity(activity, PaymentReceivedActivity::class.java)
                    }

                    val v = activity.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator

                    v.vibrate(100)

                    getSLPWallet().refreshBalance()

                    activity.refresh()
                }
            }
            wallet.addCoinsSentEventListener { wallet12, tx, prevBalance, newBalance ->
                if (!UIManager.isDisplayingDownload) {
                    activity.displayMyBalance(getBalance(wallet).toFriendlyString())
                    UIManager.showToastMessage(activity, "Sent coins!")
                    getSLPWallet().refreshBalance()
                    val intent = Intent(Constants.ACTION_CLEAR_SEND)
                    LocalBroadcastManager.getInstance(activity).sendBroadcast(intent)
                    activity.refresh()

                    UIManager.playAudio(activity, R.raw.send_coins)
                }
            }
        }

        fun setupSLPWallet(activity: MainActivity, seed: String?, hasSLPWallet: Boolean) {
            if (seed == null) {
                if (hasSLPWallet) {
                    slpWallet = SLPWallet.getInstance(activity, if (Constants.IS_PRODUCTION) Network.MAIN else Network.TEST)
                    PrefsUtil.prefs.edit().putBoolean("hasSLPWallet", true).apply()
                } else {
                    val seedFromWallet = wallet.keyChainSeed
                    val mnemonicCode = seedFromWallet.mnemonicCode
                    val recoverySeed = StringBuilder()

                    assert(mnemonicCode != null)
                    for (x in mnemonicCode!!.indices) {
                        recoverySeed.append(mnemonicCode[x]).append(if (x == mnemonicCode.size - 1) "" else " ")
                    }

                    slpWallet = SLPWallet.fromMnemonic(activity, if (Constants.IS_PRODUCTION) Network.MAIN else Network.TEST, recoverySeed.toString(), true)

                    PrefsUtil.prefs.edit().putBoolean("hasSLPWallet", true).apply()
                }
            } else {
                slpWallet = SLPWallet.fromMnemonic(activity, if (Constants.IS_PRODUCTION) Network.MAIN else Network.TEST, seed, true)
                PrefsUtil.prefs.edit().putBoolean("hasSLPWallet", true).apply()
            }

            getSLPWallet().balance.observe(activity, Observer { data -> balances = data })
        }

        fun getSLPWallet(): SLPWallet {
            return slpWallet!!
        }

        fun getBIP70Data(sendActivity: SendActivity, url: String) {
            object : Thread() {
                override fun run() {
                    try {
                        val future: ListenableFuture<PaymentSession> = PaymentSession.createFromUrl(url)

                        val session = future.get()

                        val amountWanted = session.value

                        val amountFormatted = URIHelper().processSendAmount(amountWanted.toPlainString())
                        val amountAsFloat = amountFormatted.toFloat()
                        sendActivity.runOnUiThread { sendActivity.amountText.text = amountFormatted }

                        if (amountAsFloat <= this@Companion.maximumAutomaticSend) {
                            sendActivity.runOnUiThread { sendActivity.btnSend_AM.completeSlider() }
                            send(sendActivity)
                        }
                    } catch (e: InterruptedException) {
                        e.printStackTrace()
                    } catch (e: ExecutionException) {
                        e.printStackTrace()
                    } catch (e: PaymentProtocolException) {
                        e.printStackTrace()
                    }

                }
            }.start()
        }

        fun send(sendActivity: SendActivity) {
            if (!UIManager.isDisplayingDownload) {
                sendActivity.btnSend_AM.isEnabled = false
                val amount = sendActivity.amount
                val amtDblToFrmt: Double

                amtDblToFrmt = if (!TextUtils.isEmpty(amount)) {
                    java.lang.Double.parseDouble(amount)
                } else {
                    0.0
                }

                val formatter = DecimalFormat("#.########", DecimalFormatSymbols(Locale.US))
                val amtDblFrmt = formatter.format(amtDblToFrmt)
                var amtToSend = java.lang.Double.parseDouble(amtDblFrmt)

                if (sendType == "USD" || sendType == "EUR" || sendType == "AUD") {
                    object : Thread() {
                        override fun run() {
                            if (sendType == "USD") {
                                val usdToBch = amtToSend / NetManager.price
                                bchToSend = formatter.format(usdToBch)
                                processPayment(sendActivity)
                            }

                            if (sendType == "EUR") {
                                val usdToBch = amtToSend / NetManager.priceEur
                                bchToSend = formatter.format(usdToBch)
                                processPayment(sendActivity)
                            }

                            if (sendType == "AUD") {
                                val usdToBch = amtToSend / NetManager.priceAud
                                bchToSend = formatter.format(usdToBch)
                                processPayment(sendActivity)
                            }
                        }
                    }.start()
                } else {
                    if (sendType == MonetaryFormat.CODE_BTC) {
                        println("No formatting needed")
                        bchToSend = formatter.format(amtToSend)
                    }

                    if (sendType == MonetaryFormat.CODE_MBTC) {
                        val mBTCToSend = amtToSend
                        amtToSend = mBTCToSend / 1000.0
                        bchToSend = formatter.format(amtToSend)
                    }

                    if (sendType == MonetaryFormat.CODE_UBTC) {
                        val uBTCToSend = amtToSend
                        amtToSend = uBTCToSend / 1000000.0
                        bchToSend = formatter.format(amtToSend)
                    }

                    if (sendType == "sats") {
                        val satsToSend = amtToSend.toLong()
                        amtToSend = satsToSend / 100000000.0
                        bchToSend = formatter.format(amtToSend)
                    }

                    processPayment(sendActivity)
                }
            } else {
                throwSendError(sendActivity, "Wallet hasn't finished syncing!")
            }
        }

        private fun processPayment(sendActivity: SendActivity) {
            println(bchToSend)
            if (!TextUtils.isEmpty(sendActivity.amount) && !TextUtils.isEmpty(bchToSend) && sendActivity.amountText.text != null && bchToSend != "") {
                val amtCheckVal = java.lang.Double.parseDouble(Coin.parseCoin(bchToSend).toPlainString())

                if (TextUtils.isEmpty(sendActivity.recipient)) {
                    throwSendError(sendActivity, "Please enter a recipient.")
                } else if (amtCheckVal < 0.00001) {
                    throwSendError(sendActivity, "Enter a valid amount. Minimum is 0.00001 BCH")
                } else if (this.getBalance(wallet).isLessThan(Coin.parseCoin(amtCheckVal.toString()))) {
                    throwSendError(sendActivity, "Insufficient balance!")
                } else {
                    val recipientText = sendActivity.recipient
                    val uri = URIHelper(recipientText, false)
                    val address = uri.address

                    val amount = if (uri.amount != "null")
                        uri.amount
                    else
                        "null"

                    if (amount != "null")
                        bchToSend = amount

                    if (address.startsWith("http")) {
                        sendActivity.runOnUiThread {
                            sendActivity.displayRecipientAddress(address)
                            sendActivity.sendTypeSpinner.setSelection(0)
                        }
                        this.sendType = this.displayUnits
                        PrefsUtil.prefs.edit().putString("sendType", this.sendType).apply()

                        this.processBIP70(sendActivity, address)
                    } else if (address.startsWith("+")) {
                        val rawNumber = URIHelper().getRawPhoneNumber(address)
                        val numberString = rawNumber.replace("+", "")
                        var amtToSats = java.lang.Double.parseDouble(bchToSend)
                        val satFormatter = DecimalFormat("#", DecimalFormatSymbols(Locale.US))
                        amtToSats *= 100000000
                        val sats = satFormatter.format(amtToSats).toInt()
                        val url = "https://pay.cointext.io/p/$numberString/$sats"
                        println(url)
                        this.processBIP70(sendActivity, url)
                    } else {
                        if (address.contains("#")) {
                            val toAddressFixed = EmojiParser.removeAllEmojis(address)
                            val toAddressStripped = toAddressFixed.replace("; ", "")
                            sendCoins(sendActivity, bchToSend, toAddressStripped)
                        } else {
                            try {
                                sendCoins(sendActivity, bchToSend, address)
                            } catch (e: AddressFormatException) {
                                e.printStackTrace()
                                throwSendError(sendActivity, "Invalid address!")
                            }
                        }
                    }
                }
            } else {
                throwSendError(sendActivity, "Please enter an amount.")
            }
        }

        private fun sendCoins(sendActivity: SendActivity, amount: String, toAddress: String) {
            if (toAddress.contains("#") || Address.isValidCashAddr(parameters, toAddress) || Address.isValidLegacyAddress(parameters, toAddress) && (!Address.fromBase58(MainNetParams.get(), toAddress).isP2SHAddress || WalletManager.allowLegacyP2SH)) {
                object : Thread() {
                    override fun run() {
                        val recipientAddress = sendActivity.recipient
                        val coinAmt = Coin.parseCoin(amount)

                        if (coinAmt.getValue() > 0.0) {
                            try {
                                val req: SendRequest
                                val cachedAddOpReturn = addOpReturn

                                if (useTor) {
                                    if (selectedUtxos.size == 0) {
                                        if (coinAmt == getBalance(walletKit!!.wallet())) {
                                            req = SendRequest.emptyWallet(parameters, recipientAddress, NetManager.torProxy)
                                            /*
                                                Bitcoincashj requires emptying the wallet to only have a single output for some reason.
                                                So, we cache the original setting above, then set the real setting to false here.

                                                After doing the if(addOpReturn) check below, we restore it to its actual setting.
                                            */
                                            addOpReturn = false
                                        } else {
                                            req = SendRequest.to(parameters, recipientAddress, coinAmt, NetManager.torProxy)
                                        }
                                    } else {
                                        if (coinAmt == getMaxValueOfSelectedUtxos()) {
                                            req = SendRequest.emptyWallet(parameters, recipientAddress, NetManager.torProxy)
                                            addOpReturn = false
                                        } else {
                                            req = SendRequest.to(parameters, recipientAddress, coinAmt, NetManager.torProxy)
                                        }
                                    }
                                } else {
                                    if (selectedUtxos.size == 0) {
                                        if (coinAmt == getBalance(walletKit!!.wallet())) {
                                            req = SendRequest.emptyWallet(parameters, recipientAddress)
                                            /*
                                                Bitcoincashj requires emptying the wallet to only have a single output for some reason.
                                                So, we cache the original setting above, then set the real setting to false here.

                                                After doing the if(addOpReturn) check below, we restore it to its actual setting.
                                            */
                                            addOpReturn = false
                                        } else {
                                            req = SendRequest.to(parameters, recipientAddress, coinAmt)
                                        }
                                    } else {
                                        if (coinAmt == getMaxValueOfSelectedUtxos()) {
                                            req = SendRequest.emptyWallet(parameters, recipientAddress)
                                            addOpReturn = false
                                        } else {
                                            req = SendRequest.to(parameters, recipientAddress, coinAmt)
                                        }
                                    }
                                }

                                req.ensureMinRequiredFee = false
                                req.aesKey = aesKey
                                req.utxos = selectedUtxos
                                req.feePerKb = Coin.valueOf(java.lang.Long.parseLong(1.toString() + "") * 1000L)

                                if (addOpReturn) {
                                    if (sendActivity.opReturnBox.visibility != View.GONE) {
                                        val opReturnText = sendActivity.opReturnText.text.toString()
                                        if (opReturnText.isNotEmpty()) {
                                            var scriptBuilder = ScriptBuilder().op(ScriptOpCodes.OP_RETURN)
                                            val opPushParser = OpPushParser(opReturnText)
                                            for (x in 0 until opPushParser.pushData.size) {
                                                val opPush = opPushParser.pushData[x]

                                                if (opPush.binaryData.size <= Constants.MAX_OP_RETURN) {
                                                    scriptBuilder = scriptBuilder.data(opPush.binaryData)
                                                }
                                            }

                                            req.tx.addOutput(Coin.ZERO, scriptBuilder.build())
                                        }
                                    }
                                }

                                addOpReturn = cachedAddOpReturn

                                val tx = walletKit!!.wallet().sendCoinsOffline(req)
                                val txHexBytes = Hex.encode(tx.bitcoinSerialize())
                                val txHex = String(txHexBytes, StandardCharsets.UTF_8)
                                broadcastTxToPeers(sendActivity, tx)

                                if (!useTor) {
                                    NetManager.broadcastTransaction(sendActivity, txHex, "https://rest.bitcoin.com/v2/rawtransactions/sendRawTransaction")
                                }

                                NetManager.broadcastTransaction(sendActivity, txHex, "https://rest.imaginary.cash/v2/rawtransactions/sendRawTransaction")
                            } catch (e: InsufficientMoneyException) {
                                e.printStackTrace()
                                sendActivity.runOnUiThread { e.message?.let { UIManager.showToastMessage(sendActivity, it) } }
                            } catch (e: Wallet.CouldNotAdjustDownwards) {
                                e.printStackTrace()
                                throwSendError(sendActivity, "Not enough BCH for fee!")
                            } catch (e: Wallet.ExceededMaxTransactionSize) {
                                e.printStackTrace()
                                throwSendError(sendActivity, "Transaction is too large!")
                            } catch (e: NullPointerException) {
                                e.printStackTrace()
                                throwSendError(sendActivity, "Cash Account not found.")
                            }

                        }
                    }
                }.start()
            } else if (!Address.isValidCashAddr(parameters, toAddress) || !Address.isValidLegacyAddress(parameters, toAddress)) {
                throwSendError(sendActivity, "Invalid address!")
            }
        }

        private fun processBIP70(sendActivity: SendActivity, url: String) {
            try {
                val future: ListenableFuture<PaymentSession> = PaymentSession.createFromUrl(url)

                val session = future.get()
                if (session.isExpired) {
                    throwSendError(sendActivity, "Invoice expired!")
                }

                val req = session.sendRequest
                req.aesKey = aesKey
                wallet.completeTx(req)

                val ack = session.sendPayment(ImmutableList.of(req.tx), wallet.freshReceiveAddress(), null)
                if (ack != null) {
                    Futures.addCallback<PaymentProtocol.Ack>(ack, object : FutureCallback<PaymentProtocol.Ack> {
                        override fun onSuccess(ack: PaymentProtocol.Ack?) {
                            wallet.commitTx(req.tx)
                            sendActivity.runOnUiThread {
                                sendActivity.clearSend()
                            }
                        }

                        override fun onFailure(throwable: Throwable) {
                            throwSendError(sendActivity, "An error occurred.")
                        }
                    })
                }
            } catch (e: InsufficientMoneyException) {
                throwSendError(sendActivity, "You do not have enough BCH!")
            }
        }

        fun throwSendError(sendActivity: SendActivity, message: String) {
            sendActivity.runOnUiThread {
                UIManager.showToastMessage(sendActivity, message)
                sendActivity.setSendButtonsActive()
            }
        }

        fun sendToken(sendSlpActivity: SendSLPActivity, tokenId: String, amount: BigDecimal, address: String) {
            val formattedAddress = if (!address.startsWith("simpleledger:")) "simpleledger:$address" else address

            sendSlpActivity.sendSLPBtn.isEnabled = false
            object : Thread() {
                override fun run() {

                    val trySend = getSLPWallet().sendToken(tokenId, amount, formattedAddress)

                    try {
                        trySend.subscribe(object : SingleObserver<String> {
                            override fun onSubscribe(d: Disposable) {

                            }

                            override fun onSuccess(s: String) {
                                getSLPWallet().clearSendStatus()

                                sendSlpActivity.runOnUiThread {
                                    UIManager.showToastMessage(sendSlpActivity, "Sent!")
                                    sendSlpActivity.finish()
                                }

                                getSLPWallet().refreshBalance()
                            }

                            override fun onError(e: Throwable) {
                                getSLPWallet().clearSendStatus()
                                if (e.message != null) {
                                    sendSlpActivity.runOnUiThread { UIManager.showToastMessage(sendSlpActivity, e.message!!) }
                                } else {
                                    sendSlpActivity.runOnUiThread { UIManager.showToastMessage(sendSlpActivity, "Something went wrong.") }
                                }

                                getSLPWallet().refreshBalance()
                            }
                        })
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
            }.start()
        }

        fun findBitcoinInSLPList(): BalanceInfo? {
            this.getSLPWallet().refreshBalance()

            for (coin in this.balances) {
                if (coin.tokenId == "") {
                    return coin
                }
            }

            return null
        }

        fun setAPIKey() {
            SLPWalletConfig.restAPIKey = "slpsdkH6aIcXEApC4wXQfqqPH"
        }

        fun blockieAddressFromTokenId(tokenId: String): String {
            return tokenId.slice(IntRange(12, tokenId.count() - 1))
        }

        fun isEncryptedBIP38Key(privKey: String): Boolean {
            return try {
                BIP38PrivateKey.fromBase58(this.parameters, privKey)
                true
            } catch (e: Exception) {
                false
            }
        }
    }
}